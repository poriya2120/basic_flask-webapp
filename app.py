from flask import Flask,redirect,url_for,render_template
app=Flask(__name__)
# app=App(__name__)
@app.route('/')
def helloword():
    # return 'helooword'
    return render_template("index.html")
@app.route('/login')
def login():
    # return 'helooword'
    return render_template("login.html")
# @app.route("/<name>")
# def user(name):
@app.route("/test")
def user():
    # return f"Hell{name}!"
     return render_template("test.html",number=4,ary=["poriya","taha","ali","mohammad"])
#    return render_template("test.html",content=name,number=4,ary=["poriya","taha","ali","mohammad"])
@app.route("/admin")
def admin():
    return redirect(url_for("helloword"))
    #  return redirect(url_for("user",name="admin"))
if __name__=="__main__":
    app.run(debug=True)